# Example multi-stage Dockerfile for Gradle and Java.
# In this example we use 2 stages:
#    - Build & Test: Builds our Java application using gradle and executes the unit tests.
#    - Runtime: Packages the application built in the previous stage together with the Java runtime.
# The advantage of this approach is that the final runtime image doesn't contain the build tools.
# Therefore, the image is smaller and more secure!

#################################################################
# Stage 1: BUILD & TEST                                         #
#################################################################
FROM gradle:6.7.0-jdk15 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

#################################################################
# Stage 2: RUNTIME                                              #
#################################################################
FROM adoptopenjdk/openjdk11:alpine-jre
EXPOSE 8081

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.7.3/wait /wait
RUN chmod +x /wait

RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
COPY --from=build /home/gradle/src/build/libs/*.jar app.jar
CMD ./wait && java -jar app.jar --server.port=8081

#ENTRYPOINT ["java", "-jar", "app.jar"]

#################################################################
# TESTING                                                       #
#################################################################
# docker build -t user-mgmt-service -f Dockerfile .
# docker run -p 8081:8081 user-mgmt-service