# user-mgmt-microservice

### Requirements (Tested)

- Java SDK 15.0.1
- Docker version 19.03.13

### Application environment

- Java version 11
- Build tool gradle
- Keycloak (Identity and Access Management) using JWT and OpenID Connect

### Setup your local user mgmt microservice

1. Clone user-mgmt-service
2. Run docker-compose.yml
    - Starts Keycloak on port 8080
        - Keycloak is accessible via http://localhost:8080/auth/ (Documentation and administration console can be found here)
        - Optional:
            * Configurations can be done via the administration console (username: admin, password: NotSecure)
            * Administration console (username/password) will be deactivated in production use (will be replaced by keycloak jwt token handling also)
    - Starts UserManagementAPI on port 8081 (Automatically configures Keycloak)
3. Your docker environment is ready for use now
    - Check out section "API documentation" and learn how to use the UserManagementAPI

### API documentation

1. Download / check out UserManagementAPI Documentation
    * 1.1. (JSON): [Postman Collection](user-mgmt.postman_collection.json)
        - 1.1.1. Import into Postman: https://learning.postman.com/docs/getting-started/importing-and-exporting-data/#importing-data-into-postman
        - 1.1.2. Show API Documentation in Web: https://learning.postman.com/docs/publishing-your-api/documenting-your-api/#documenting-an-existing-collection
    * 1.2. (YAML): [Swagger OpenApi v3](SwaggerDocu.yaml)

### Verify token signature

1. Use verify endpoint of UserManagementAPI
    * Request Body:
        - keycloakJavaWebToken: Should be given by the user (For Testing: Use login endpoint - result is a token)
        - publicKey: Can be found via http://localhost:8080/auth/admin/master/console/#/realms/user-mgmt/keys
2. If Response is "true" token is valid
    * <b>Be careful:</b> Token is also invalid if it has expired, not only if the signature verification failed

### Responsible

* Marco Ebster, BSc
