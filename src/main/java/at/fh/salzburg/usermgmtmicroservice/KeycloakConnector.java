package at.fh.salzburg.usermgmtmicroservice;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;

/**
 * Class manages keycloak connections (connect/disconnect)
 */
public class KeycloakConnector
{
    /**
     * Method to connect to keycloak
     * @return Keycloak instance
     */
    public Keycloak connectToKeycloak()
    {
        return KeycloakBuilder.builder()
                .serverUrl("http://user-mgmt-app:8080/auth/")
                .realm("master")
                .username("admin")
                .password("NotSecure")
                .clientId("admin-cli")
                .resteasyClient(
                        new ResteasyClientBuilder()
                                .connectionPoolSize(10).build()
                ).build();
    }

    /**
     * Method to disconnect from keycloak
     * @param kc Keycloak instance
     */
    public void closeKeycloakConnection(Keycloak kc)
    {
        kc.close();
    }
}
