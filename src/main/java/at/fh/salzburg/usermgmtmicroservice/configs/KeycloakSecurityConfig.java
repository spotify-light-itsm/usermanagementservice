package at.fh.salzburg.usermgmtmicroservice.configs;

import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

/**
 * Class manages token authorization and role handling
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class KeycloakSecurityConfig extends KeycloakWebSecurityConfigurerAdapter
{

    /**
     * Method defines which endpoints need which roles for use
     * @param http HTTP Web based security configuration for specific requests
     * @throws Exception default
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        super.configure(http);
        http.authorizeRequests()
                .antMatchers("/auth/register").permitAll()
                .antMatchers("/auth/login").permitAll()
                .antMatchers("/auth/verify").permitAll()
                .antMatchers("/auth/test/role/user").hasAnyRole("user")
                .antMatchers("/auth/test/role/admin").hasAnyRole("admin")
                .anyRequest()
                .permitAll();
        http.csrf().disable();
    }

    /**
     * Method sets keycloak as global authentication provider
     * @param auth Adds authentication provider
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
    {
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    /**
     * Method defines an session authentication strategy
     * @return SessionAuthenticationStrategy instance
     */
    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy()
    {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    /**
     * Method defines an keycloak config resolver
     * @return KeycloakConfigResolver instance
     */
    @Bean
    public KeycloakConfigResolver KeycloakConfigResolver()
    {
        return new KeycloakSpringBootConfigResolver();
    }
}