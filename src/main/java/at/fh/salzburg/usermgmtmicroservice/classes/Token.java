package at.fh.salzburg.usermgmtmicroservice.classes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class to manage keycloak tokens
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Token
{
    private String keycloakJavaWebToken;
    private String publicKey;
}
