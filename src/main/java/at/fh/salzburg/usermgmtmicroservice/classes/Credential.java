package at.fh.salzburg.usermgmtmicroservice.classes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class to manage user credentials
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Credential
{
    private String username;
    private String password;
}
