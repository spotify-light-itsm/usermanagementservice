package at.fh.salzburg.usermgmtmicroservice.classes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class to manage users
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User
{
    private String firstName;
    private String lastName;
    private String eMail;
    private String userName;
    private String password;
}
