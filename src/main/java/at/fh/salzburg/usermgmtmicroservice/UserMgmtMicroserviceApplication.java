package at.fh.salzburg.usermgmtmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Spring Boot App Main Class
 */
@EnableEurekaClient
@SpringBootApplication
public class UserMgmtMicroserviceApplication
{
    /**
     * Spring Boot Main Method
     * @param args spring boot app cli arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(UserMgmtMicroserviceApplication.class, args);
    }
}
