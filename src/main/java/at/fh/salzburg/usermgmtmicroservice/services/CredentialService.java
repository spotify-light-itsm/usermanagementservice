package at.fh.salzburg.usermgmtmicroservice.services;

import at.fh.salzburg.usermgmtmicroservice.classes.Credential;
import org.json.JSONException;

import java.io.IOException;

/**
 * Interfaces for CredentialServiceImplementation (defines methods, which are designated for credentials)
 */
public interface CredentialService
{
    /**
     * Login a user
     * @param credential user credentials
     * @return JWT token
     * @throws IOException default
     * @throws JSONException default
     */
    String login(Credential credential) throws IOException, JSONException;
}
