package at.fh.salzburg.usermgmtmicroservice.services;

import at.fh.salzburg.usermgmtmicroservice.classes.User;

/**
 * Interfaces for UserServiceImplementation (defines methods, which are designated for users)
 */
public interface UserService
{
    /**
     * Register a user
     * @param user User instance
     */
    void register(User user);
}
