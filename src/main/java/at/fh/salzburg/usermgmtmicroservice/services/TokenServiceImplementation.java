package at.fh.salzburg.usermgmtmicroservice.services;

import at.fh.salzburg.usermgmtmicroservice.classes.Token;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.JWTVerifier;
import org.springframework.stereotype.Service;

import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * Implements TokenService interface
 */
@Service
public class TokenServiceImplementation implements TokenService
{
    /**
     * Verify a token
     * @param token JWT token
     * @return bool
     */
    @Override
    public Boolean verify(Token token)
    {
        try
        {
            // Choose encryption protocol
            KeyFactory kf = KeyFactory.getInstance("RSA");
            // Decode public key
            X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(token.getPublicKey()));
            // Convert to RSA public key
            RSAPublicKey publicKey = (RSAPublicKey) kf.generatePublic(keySpecX509);
            // Initialize algorithm
            Algorithm algorithm = Algorithm.RSA256(publicKey, null);
            // Initialize JWT verifier
            JWTVerifier verifier = JWT.require(algorithm).build();
            // Verify JWT
            verifier.verify(token.getKeycloakJavaWebToken());
            return Boolean.TRUE;
        }
        catch (Exception e)
        {
            return Boolean.FALSE;
        }
    }
}
