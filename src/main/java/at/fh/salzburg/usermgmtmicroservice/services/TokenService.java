package at.fh.salzburg.usermgmtmicroservice.services;

import at.fh.salzburg.usermgmtmicroservice.classes.Token;

/**
 * Interfaces for TokenServiceImplementation (defines methods, which are designated for tokens)
 */
public interface TokenService
{
    /**
     * Verify a token
     * @param token JWT token
     * @return bool
     */
    Boolean verify(Token token);
}
