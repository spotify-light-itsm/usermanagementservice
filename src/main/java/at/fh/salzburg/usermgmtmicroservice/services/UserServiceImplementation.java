package at.fh.salzburg.usermgmtmicroservice.services;

import at.fh.salzburg.usermgmtmicroservice.KeycloakConnector;
import at.fh.salzburg.usermgmtmicroservice.classes.User;
import at.fh.salzburg.usermgmtmicroservice.InitKeycloak;
import org.keycloak.admin.client.Keycloak;
import org.springframework.stereotype.Service;

/**
 * Implements UserService interface
 */
@Service
public class UserServiceImplementation implements UserService
{
    /**
     * Register a user
     * @param user User instance
     */
    @Override
    public void register(User user)
    {
        // Connect to keycloak
        KeycloakConnector kcc = new KeycloakConnector();
        Keycloak kc = kcc.connectToKeycloak();
        InitKeycloak keycloak = new InitKeycloak();
        // Define default user role
        String[] user_roles = {"user"};
        // Create new user
        keycloak.createNewUserWithRoles(kc, user.getUserName(), user.getPassword(), user.getFirstName(), user.getLastName(), user.getEMail(), user_roles);
        // Disconnect from keycloak
        kcc.closeKeycloakConnection(kc);
    }
}
