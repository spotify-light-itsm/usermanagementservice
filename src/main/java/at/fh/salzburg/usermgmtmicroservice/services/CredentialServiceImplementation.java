package at.fh.salzburg.usermgmtmicroservice.services;

import at.fh.salzburg.usermgmtmicroservice.classes.Credential;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implements CredentialService interface
 */
@Service
public class CredentialServiceImplementation implements CredentialService
{
    // Read value from application.properties
    @Value("${keycloak.auth-server-url}")
    private String auth_server_url;

    // Read value from application.properties
    @Value("${keycloak.resource}")
    private String client_id;

    /**
     * Login a user
     * @param credential user credentials
     * @return JWT token
     * @throws IOException default
     * @throws JSONException default
     */
    @Override
    public String login(Credential credential) throws IOException, JSONException
    {
        // Configure HTTP POST request
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(auth_server_url + "/realms/user-mgmt/protocol/openid-connect/token");
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("grant_type", "password"));
        nameValuePairs.add(new BasicNameValuePair("username", credential.getUsername()));
        nameValuePairs.add(new BasicNameValuePair("password", credential.getPassword()));
        nameValuePairs.add(new BasicNameValuePair("client_id", client_id));
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        httpPost.setHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded"));

        // Execute HTTP request
        CloseableHttpResponse response = httpClient.execute(httpPost);
        // Convert HTTP response (JWT) to JSON object
        JSONObject json_auth = new JSONObject(EntityUtils.toString(response.getEntity()));
        // Return JSON object
        return json_auth.toString();
    }
}
