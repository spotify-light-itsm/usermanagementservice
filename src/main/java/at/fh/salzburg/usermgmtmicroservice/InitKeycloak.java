package at.fh.salzburg.usermgmtmicroservice;

import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class manages keycloak initialization (basic config) on start up
 */
@Slf4j
@Component
public class InitKeycloak implements CommandLineRunner
{

    /**
     * Method creates a new realm (area) and a new client (app) on keycloak
     * @param kc keycloak instance (use KeycloakConnector)
     * @param realm_id name of realm
     * @param client_id name of client
     * @param redirectUri uri to redirect after successful authorization
     * @param clientRoles roles which client should be able to handle
     */
    public void createNewRealmAndClient(Keycloak kc, String realm_id, String client_id, String redirectUri, String[] clientRoles)
    {
        RealmRepresentation realm = new RealmRepresentation();
        realm.setRealm(realm_id);
        realm.setEnabled(true);
        realm.setClients(createNewClient(client_id, redirectUri));

        kc.realms().create(realm);
        createClientRoles(kc, realm_id, client_id, clientRoles);
    }

    /**
     * Helper for createNewRealmAndClient method (handles client creation)
     * @param client_id name of client
     * @param redirectUri uri to redirect after successful authorization
     * @return list of ClientRepresentation instances
     */
    public List<ClientRepresentation> createNewClient(String client_id, String redirectUri)
    {
        ClientRepresentation client = new ClientRepresentation();
        client.setClientId(client_id);
        client.setDirectAccessGrantsEnabled(true);
        client.setPublicClient(true);
        List<String> redirectUris = new ArrayList<>();
        redirectUris.add(redirectUri);
        client.setRedirectUris(redirectUris);
        List<ClientRepresentation> clients = new ArrayList<>();
        clients.add(client);
        return clients;
    }

    /**
     * Helper for createNewUserWithRoles method (handles user creation)
     * @param userName name of the user
     * @param password password of the user
     * @param firstName first name of the user
     * @param lastName last name of the user
     * @param eMail email of the user
     * @return UserRepresentation instance
     */
    public UserRepresentation createNewUser(String userName, String password, String firstName, String lastName, String eMail)
    {
        UserRepresentation user = new UserRepresentation();
        user.setUsername(userName);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(eMail);
        user.setEnabled(true);
        user.setEmailVerified(true);

        List<CredentialRepresentation> credentials = new ArrayList<>();
        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue(password);
        credentials.add(credential);
        user.setCredentials(credentials);
        return user;
    }

    /**
     * Method creates a new user with roles on keycloak
     * @param kc keycloak instance (use KeycloakConnector)
     * @param userName name of the user
     * @param password password of the user
     * @param firstName first name of the user
     * @param lastName last name of the user
     * @param eMail email of the user
     * @param roles role of the user
     */
    public void createNewUserWithRoles(Keycloak kc, String userName, String password, String firstName, String lastName, String eMail, String[] roles)
    {

        UserRepresentation user = createNewUser(userName, password, firstName, lastName, eMail);

        RealmResource realmResource = kc.realm("user-mgmt");
        UsersResource usersResource = realmResource.users();

        Response response = usersResource.create(user);
        String userId = CreatedResponseUtil.getCreatedId(response);

        ClientRepresentation appClient = realmResource.clients().findByClientId("auth-app").get(0);

        for (String role : roles)
        {
            RoleRepresentation userClientRole = realmResource.clients().get(appClient.getId()).roles().get(role).toRepresentation();
            usersResource.get(userId).roles().clientLevel(appClient.getId()).add(Arrays.asList(userClientRole));
        }
    }

    /**
     * Helper for createNewRealmAndClient method (handles client role setup)
     * @param kc keycloak instance (use KeycloakConnector)
     * @param realm_id name of realm
     * @param client_id name of client
     * @param roles roles which client should be able to handle
     */
    public void createClientRoles(Keycloak kc, String realm_id, String client_id, String[] roles)
    {
        for (String role : roles)
        {
            RoleRepresentation userRoleRepresentation = new RoleRepresentation();
            userRoleRepresentation.setName(role);
            userRoleRepresentation.setClientRole(true);
            kc.realm(realm_id).clients().findByClientId(client_id).forEach(clientRepresentation ->
                    kc.realm(realm_id).clients().get(clientRepresentation.getId()).roles().create(userRoleRepresentation));
        }
    }

    /**
     * Override spring boot method run, to initialize keycloak on app start up
     * @param args spring boot app cli arguments
     */
    @Override
    public void run(String... args)
    {
        try
        {
            log.info("Wait for Keycloak to initialize...");
            // Connect to keycloak
            KeycloakConnector kcc = new KeycloakConnector();
            Keycloak kc = kcc.connectToKeycloak();
            // Define roles
            String[] no_roles = {};
            String[] user_roles = {"user"};
            String[] admin_roles = {"admin"};
            String[] all_roles = {"admin", "user"};
            // Create realm and client
            createNewRealmAndClient(kc, "user-mgmt", "auth-app", "http://localhost:8081/auth/*", all_roles);
            // Create users
            createNewUserWithRoles(kc, "no_role", "NotSecure", "Herbert", "Mayer", "herbert.mayer@test.at", no_roles);
            createNewUserWithRoles(kc, "user_role","NotSecure", "Markus", "Schmidt", "markus.schmidt@test.at", user_roles);
            createNewUserWithRoles(kc, "admin_role","NotSecure", "Alex", "Huber", "alex.huber@test.at", admin_roles);
            createNewUserWithRoles(kc, "all_role","NotSecure", "Johann", "Stein", "johann.stein@test.at", all_roles);
            // Disconnect from keycloak
            kcc.closeKeycloakConnection(kc);
            log.info("Keycloak initialization has been done.");
        }
        catch(Exception e)
        {
            if (e.getMessage().equals("HTTP 409 Conflict"))
            {
                log.info("Keycloak has already been initialized.");
            }
            else
            {
                e.printStackTrace();
            }
        }
    }
}