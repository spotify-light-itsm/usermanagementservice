package at.fh.salzburg.usermgmtmicroservice.controllers;

import at.fh.salzburg.usermgmtmicroservice.classes.Credential;
import at.fh.salzburg.usermgmtmicroservice.classes.Token;
import at.fh.salzburg.usermgmtmicroservice.classes.User;
import at.fh.salzburg.usermgmtmicroservice.services.CredentialService;
import at.fh.salzburg.usermgmtmicroservice.services.TokenService;
import at.fh.salzburg.usermgmtmicroservice.services.UserService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

/**
 * REST interface for user management
 */
@RestController
@RequestMapping("/auth")
public class AuthController
{
    private final CredentialService credentialService;
    private final UserService userService;
    private final TokenService tokenService;

    /**
     * Constructor of AuthController (linking of services)
     * @param credentialService handles user credentials
     * @param userService handles users
     * @param tokenService handles tokens
     */
    @Autowired
    public AuthController(CredentialService credentialService, UserService userService, TokenService tokenService)
    {
        this.credentialService = credentialService;
        this.userService = userService;
        this.tokenService = tokenService;
    }

    /**
     * Register a user
     * @param user body must be a valid User instance representation
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void register(@RequestBody @Valid User user) { userService.register(user); }

    /**
     * Login a user
     * @param credential body must be a valid Credential instance representation
     * @return JWT token
     * @throws IOException default
     * @throws JSONException default
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@RequestBody @Valid Credential credential) throws IOException, JSONException { return credentialService.login(credential); }

    /**
     * Verify a token (signature verification and expiration)
     * @param token JWT token to validate
     * @return bool - true (valid) or false (invalid)
     */
    @RequestMapping(value = "/verify", method = RequestMethod.POST)
    public Boolean verify(@RequestBody @Valid Token token) { return tokenService.verify(token); }

    /**
     * Test if user has role user (give token in AuthorizationHeader of request)
     * @return http response code - (e.g. 200 OK or 403 Forbidden)
     */
    @RequestMapping(value = "test/role/user", method = RequestMethod.GET)
    public ResponseEntity<String> getUser() {
        return ResponseEntity.ok("Hello User");
    }

    /**
     * Test if user has role admin (give token in AuthorizationHeader of request)
     * @return http response code - (e.g. 200 OK or 403 Forbidden)
     */
    @RequestMapping(value = "test/role/admin", method = RequestMethod.GET)
    public ResponseEntity<String> getAdmin() {
        return ResponseEntity.ok("Hello Admin");
    }
}